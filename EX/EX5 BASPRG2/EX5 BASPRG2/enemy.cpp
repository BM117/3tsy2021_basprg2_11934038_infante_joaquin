#include "enemy.h"

enemy::enemy(namesList allNames, int amp) {
	//Base Stats for all enemy classes
	eHP = 20;
	eMaxHP = 20;
	ePOW = 3;
	eVIT = 3;
	eAGI = 3;
	eDEX = 3;
	//Stat bonuses for all enemy classes
	int classpicker = rand() % 3 + 1;


	float amplifier = 1.0;

	if ((amp % 5) == 0) {
		amplifier += 0.5;
	}

	switch (classpicker) {
	case 1:
		eClass = unitType::warrior;
		eHP += (20 * amplifier);
		eMaxHP += (20 * amplifier);
		ePOW += (1 * amplifier);
		eVIT += (2 * amplifier);

		break;
	case 2:
		eClass = unitType::assassin;
		ePOW += (2 * amplifier);
		eAGI += (2 * amplifier);
		eDEX += (1 * amplifier);

		break;
	case 3:
		eClass = unitType::mage;
		ePOW += (3 * amplifier);
		eDEX += (2 * amplifier);
		break;
	}

	//Random Name picker
	int namepicker = rand() % 15;

	switch (eClass) {
	case unitType::warrior:
		eName = allNames.getWarriorNames()[namepicker];
		break;
	case unitType::assassin:
		eName = allNames.getAssassinNames()[namepicker];
		break;
	case unitType::mage:
		eName = allNames.getMageNames()[namepicker];
		break;
	default:
		eName = "Bob";
		break;
	}
}

string enemy::getName()
{
	return eName;
}

unitType enemy::getClass()
{
	return eClass;
}

string enemy::getClassName()
{
	switch (eClass) {
	case unitType::warrior:
		return "Warrior";
		break;
	case unitType::mage:
		return "Mage";
		break;
	case unitType::assassin:
		return "Assassin";
		break;
	default:
		return "How did you even...";
	}
}

int enemy::getHP()
{
	return eHP;
}

int enemy::getPOW()
{
	return ePOW;
}

int enemy::getVIT()
{
	return eVIT;
}

int enemy::getAGI()
{
	return eAGI;
}

int enemy::getDEX()
{
	return eDEX;
}

void enemy::setHP(int reducValue)
{
	eHP -= reducValue;
}
//Printing stats
void enemy::printStats()
{
	cout << "Class: " << getClassName() << endl;
	cout << "HP: " << eHP << endl;
	cout << "POW: " << ePOW << endl;
	cout << "VIT: " << eVIT << endl;
	cout << "AGI: " << eAGI << endl;
	cout << "DEX: " << eDEX << endl;
}
//Damage bonuses/debuffs
int enemy::attack(int pAGI, int pVIT, unitType pClass)
{
	int hitRate = (eDEX / pAGI) * 100;
	int dmg = ePOW - pVIT;

	switch (eClass) {
	case unitType::warrior:
		if (pClass == unitType::assassin) {
			dmg *= 1.5;
		}
		else if (pClass == unitType::mage) {
			dmg *= 0.5;
		}
		break;

	case unitType::assassin:
		if (pClass == unitType::warrior) {
			dmg *= 0.5;
		}
		else if (pClass == unitType::mage) {
			dmg *= 1.5;
		}
		break;

	case unitType::mage:
		if (pClass == unitType::warrior) {
			dmg *= 1.5;
		}
		else if (pClass == unitType::assassin) {
			dmg *= 0.5;
		}
		break;
	}


	if (hitRate > 80) {
		hitRate = 80;
	}
	else if (hitRate < 20) {
		hitRate = 20;
	}



	if (rand() % 100 + 1 > hitRate) {
		dmg = 0;
	}

	if (dmg <= 0) {
		dmg = 1;
	}

	return dmg;
}