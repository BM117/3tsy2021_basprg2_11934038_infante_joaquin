#include "unit.h"

player::player(string name, unitType Klass) {
	pName = name;
	pClass = Klass;
	//Base stats for all player classes
	pHP = 50;
	pMaxHP = 50;
	pPOW = 5;
	pVIT = 5;
	pAGI = 5;
	pDEX = 5;
	//Stat bonuses for each class
	switch (Klass) {
	case unitType::warrior:

		pMaxHP += 30;
		pHP += 30;
		pPOW += 1;
		pVIT += 3;

		break;
	case unitType::assassin:

		pPOW += 2;
		pAGI += 3;
		pDEX += 3;

		break;
	case unitType::mage:

		pPOW += 5;
		pDEX += 3;

		break;
	}



}
//Growth Stats when the player wins
void player::growth() {

	int healFactor = 0;

	switch (pClass) {

	case unitType::warrior:
		healFactor = 24;
		pPOW += 3;
		pVIT += 3;
		if (pHP + healFactor > pMaxHP) {
			healFactor = pMaxHP - pHP;
		}
		pHP += healFactor;
		cout << "You heal " << healFactor << " HP" << endl;
		cout << "Power has increased by: " << 3 << endl;
		cout << "Vitality has increased by: " << 3 << endl;

		break;
	case unitType::assassin:
		healFactor = 15;
		pAGI += 3;
		pDEX += 3;
		if (pHP + healFactor > pMaxHP) {
			healFactor = pMaxHP - pHP;
		}
		pHP += healFactor;
		cout << "You heal " << healFactor << " HP" << endl;
		cout << "Agility has increased by: " << 3 << endl;
		cout << "Dexterity has increased by: " << 3 << endl;

		break;
	case unitType::mage:
		healFactor = 15;
		pPOW += 5;
		if (pHP + healFactor > pMaxHP) {
			healFactor = pMaxHP - pHP;
		}
		pHP += healFactor;
		cout << "You heal " << healFactor << " HP" << endl;
		cout << "Power has increased by: " << 5 << endl;

		break;
	}
}

string player::getName()
{
	return pName;
}

unitType player::getClass()
{
	return pClass;
}

string player::getClassName()
{
	switch (pClass) {
	case unitType::warrior:
		return "Warrior";
		break;
	case unitType::mage:
		return "Mage";
		break;
	case unitType::assassin:
		return "Assassin";
		break;
	default:
		return "N/A";
		break;
	}
}

int player::getHP()
{
	return pHP;
}

int player::getMaxHP() {
	return pMaxHP;
}

int player::getPOW()
{
	return pPOW;
}

int player::getVIT()
{
	return pVIT;
}

int player::getAGI()
{
	return pAGI;
}

int player::getDEX()
{
	return pDEX;
}

void player::setHP(int reducValue)
{
	pHP -= reducValue;
}
//Printing stats
void player::printStats()
{
	cout << "Class: " << getClassName() << endl;
	cout << "HP: " << pHP << endl;
	cout << "POW: " << pPOW << endl;
	cout << "VIT: " << pVIT << endl;
	cout << "AGI: " << pAGI << endl;
	cout << "DEX: " << pDEX << endl;
}
//Damage bonuses/debuffs
int player::attack(int eAGI, int eVIT, unitType eClass)
{
	int hitRate = (pDEX / eAGI) * 100;
	int dmg = pPOW - eVIT;

	switch (pClass) {
	case unitType::warrior:
		if (eClass == unitType::assassin) {
			dmg *= 1.5;
		}
		else if (eClass == unitType::mage) {
			dmg *= 0.5;
		}
		break;

	case unitType::assassin:
		if (eClass == unitType::warrior) {
			dmg *= 0.5;
		}
		else if (eClass == unitType::mage) {
			dmg *= 1.5;
		}
		break;

	case unitType::mage:
		if (eClass == unitType::warrior) {
			dmg *= 1.5;
		}
		else if (eClass == unitType::assassin) {
			dmg *= 0.5;
		}
		break;
	}

	//hitrate
	if (hitRate > 80) {
		hitRate = 80;
	}
	else if (hitRate < 20) {
		hitRate = 20;
	}

	int hitRoll = rand() % 100 + 1;

	if (dmg <= 0) {
		dmg = 1;
	}

	if (hitRoll > hitRate) {
		dmg = 0;
	}

	return dmg;
}