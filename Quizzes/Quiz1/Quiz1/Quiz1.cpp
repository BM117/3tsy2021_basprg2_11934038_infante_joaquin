#include <iostream>
#include <ctime>
#include <vector>
#include <stdlib.h>

using namespace std;

void playRound(int* round, int* moneyEarned, int* mmLeft) {
	//First step, distribute cards for hand
	int pickedSide = 0;
	int side = 0;
	vector<int> handKaiji;
	vector<int> handTonegawa;

	cout << "Choose your side..." << endl;
	cout << "[1] Emperor" << endl;
	cout << "[2] Slave" << endl;
	cout << "[3] Random" << endl;
	cout << "> ";
	cin >> pickedSide;

	switch (pickedSide) {
	case 1:
		side = 1;
		break;
	case 2:
		side = 2;
		break;
	case 3:
		side = rand() % 2 + 1;
		break;
	}

	cout << "You have selected ";
	switch (side) {
	case 1:
		cout << "Emperor" << endl;
		break;
	case 2:
		cout << "Slave" << endl;
		break;
	}

	//Legend:
	// 3 -> Emperor
	// 2 -> Civilian
	// 1 -> Slave

	for (int i = 0; i < 4; i++) {
		handKaiji.push_back(2);
		handTonegawa.push_back(2);
	}

	switch (side) {
	case 1:
		handKaiji.push_back(3);
		handTonegawa.push_back(1);
		break;
	case 2:
		handKaiji.push_back(1);
		handTonegawa.push_back(3);
		break;
	}

	int cardPicked = 0;
	int enemyCard = 0;
	bool finalPlay = false;

	bool win = false;
	//Second, loop till Emperor or slave is played

	int mmBet = 0;

	cout << "How many milimetres will you bet?" << endl;
	cout << "> ";
	cin >> mmBet;

	cout << "Emperor side goes first" << endl;
	while (!finalPlay) {


		switch (side) {
		case 1:
			//The case where the player chose the Emperor side, fill this
			do {
				enemyCard = handTonegawa[rand() % 3];
			} while (cardPicked == 1);

			break;
		case 2:
			//Basic randomizer that loops till it picks a valid card
			do {
				enemyCard = handTonegawa[rand() % 5];
			} while (enemyCard == 0);

			cout << "Debug: Tonegawa plays " << enemyCard << endl;

			cout << "Choose a card to play" << endl;
			for (int i = 0; i < handKaiji.size(); i++) {
				cout << "[" << i << "] - ";
				switch (handKaiji[i]) {
				case 1:
					cout << "Slave" << endl;
					break;
				case 2:
					cout << "Citizen" << endl;
					break;
				case 3:
					cout << "Emperor" << endl;
					break;
				}
			}
			cout << "> ";
			cin >> cardPicked;
			break;
		}
		if (cardPicked == enemyCard) {
			cout << "Tie" << endl;
		}
		else {
			switch (cardPicked) {
			case 1:
				if (enemyCard == 2) {
					cout << "The civilian kills the slave" << endl;
					cout << "Player Loses" << endl;
					win = false;
				}
				else if (enemyCard == 3) {
					cout << "The slave kills the emperor" << endl;
					cout << "Player Wins" << endl;
					win = true;
				}
				break;
			case 2:
				if (enemyCard == 1) {
					cout << "The civilian kills the slave" << endl;
					cout << "Player Wins" << endl;
					win = true;
				}
				else if (enemyCard == 3) {
					cout << "The emperor kills the civilian" << endl;
					cout << "Player loses" << endl;
					win = false;
				}
				break;
			case 3:
				if (enemyCard == 1) {
					cout << "The slave kills the emperor" << endl;
					cout << "Player loses" << endl;
					win = false;
				}
				else if (enemyCard == 2) {
					cout << "The emperor kills the civilian" << endl;
					cout << "Player wins" << endl;
					win = true;
				}
				break;
			}
			finalPlay = true;
		}
	}
	//update moneyEarned or mmLeft

	int money;

	if (win) {
		money = mmBet * 100 * 100;
		if (side == 2) {
			money = money * 5;
		}
		cout << "You have earned " << money << "Yen" << endl;

		*moneyEarned += money;
		cout << "Total: " << *moneyEarned << "Y" << endl;
	}
	else { //lose condition
		*mmLeft -= mmBet;
		cout << "You have " << *mmLeft << " milimetres left" << endl;
	}
}

int main() {
	srand(time(0));

	int round = 1;
	int mmLeft = 30;
	int moneyEarned = 0;

	while (true) {
		cout << "Round: " << round << endl;
		playRound(&round, &moneyEarned, &mmLeft);
		round++;
	}

	cout << endl;
	return 0;
}
