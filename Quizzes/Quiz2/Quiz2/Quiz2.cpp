#include <iostream>
#include <string>
#include <math.h>

using namespace std;
//Node struct
struct Node
{
	string name;
	Node* next = NULL;
	Node* previous = NULL;
};
//Main code block
int main() {

	Node* head = nullptr;
	Node* current = nullptr;
	Node* previous = nullptr;
	// amount of soldiers at the wall
	int soldierAmount = 0;
	//choosing how many soldiers are at the wall currently
	cout << "Choose how many soldiers there are at the wall:" << endl;
	cin >> soldierAmount;

	// terminating condition to make sure the code only counts how many soldiers you placeed
	for (int i = 0; i < soldierAmount; i++) {
		//asking for the names of the soldiers
		string soldierName;
		cout << i << endl;
		cout << "What is this soldier's name?" << endl;
		cout << "> ";
		cin >> soldierName;
		cout << endl;
		// circular linklist
		if (i == 0) {
			current = new Node;
			current->name = soldierName;
			head = current;
			previous = current;
		}
		else if (i == soldierAmount - 1) {
			current = new Node;
			current->name = soldierName;
			current->next = head;
			current->previous = previous;
			previous->next = current;
		}
		else {
			current = new Node;
			previous->next = current;
			current->previous = previous;
			current->name = soldierName;
			previous = current;
		}
	}

	int remaining = soldierAmount;
	int round = 1;
	//Code for randomzing the soldiers picked
	do {
		cout << "=========================================================" << endl;
		cout << "Round " << round << endl;
		cout << "=========================================================" << endl;

		cout << "Remaining members: " << endl;
		//print remaining members
		Node* counterCurrent = head;

		for (int i = 0; i < remaining; i++) {
			cout << counterCurrent->name << endl;
			counterCurrent = counterCurrent->next;
		}
		//generates the random number the soldiers pick
		int rando = rand() % remaining;

		cout << head->name << " has rolled " << rando + 1 << endl;

		for (int i = 0; i < rando + 1; i++) {
			head = head->next;
		}

		cout << head->name << " has to stay and defend the wall" << endl;

		//set previous to current head because current head is the one that is to be eliminated
		previous = head;
		//set head to the node right next to head
		head = head->next;
		//set previous to the node before it
		previous = previous->previous;
		//set the new previous' next to head
		previous->next = head;
		//set head's previous to previous
		head->previous = previous;

		round++;
		remaining--;
	} while (remaining > 1);
	//end of code
	return 0;
}