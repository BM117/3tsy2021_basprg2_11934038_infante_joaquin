// First Fantasy.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "player.h" 
#include "enemy.h"
#include "namesList.h"

player battle(player you, enemy opponent) {

	cout << "Loaded Player: " << you.getName() << endl;
	you.printStats();
	//execution waiting here
	cout << "Loaded opponent: " << opponent.getName() << endl;
	opponent.printStats();

	cout << endl;

	cout << "The battle begins!" << endl;

	//pause exection

	int dmg = 0;

	if (you.getAGI() >= opponent.getAGI()) {
		//turn cycle favours you
		while (you.getHP() >= 0 && opponent.getHP() >= 0) {
			dmg = 0;

			dmg = you.attack(opponent.getAGI(), opponent.getVIT(), opponent.getClass());

			if (dmg <= 0) {
				cout << you.getName() << "'s attack misses!" << endl;
			}
			else {
				cout << you.getName() << " strikes for " << dmg << " damage!" << endl;
			}

			opponent.setHP(dmg);
			cout << opponent.getName() << " HP: " << opponent.getHP() << endl;

			if (opponent.getHP() <= 0) {
				break;
			}

			dmg = opponent.attack(you.getAGI(), you.getVIT(), you.getClass());

			if (dmg <= 0) {
				cout << opponent.getName() << "'s attack misses!" << endl;
			}
			else {
				cout << opponent.getName() << " strikes for " << dmg << " damage!" << endl;
			}

			you.setHP(dmg);

			cout << you.getName() << " HP: " << you.getHP() << endl;
			cout << "--------------------------------------------------------------" << endl;
		}
	}
	else {
		//turn cycle favours enemy

		while (you.getHP() >= 0 && opponent.getHP() >= 0) {
			dmg = 0;

			dmg = opponent.attack(you.getAGI(), you.getVIT(), you.getClass());

			if (dmg <= 0) {
				cout << opponent.getName() << "'s attack misses!" << endl;
			}
			else {
				cout << opponent.getName() << " strikes for " << dmg << " damage!" << endl;
			}

			you.setHP(dmg);
			cout << you.getName() << " HP: " << you.getHP() << endl;

			if (you.getHP() <= 0) {
				break;
			}

			dmg = you.attack(opponent.getAGI(), opponent.getVIT(), opponent.getClass());

			if (dmg <= 0) {
				cout << you.getName() << "'s attack misses!" << endl;
			}
			else {
				cout << you.getName() << " strikes for " << dmg << " damage!" << endl;
			}
		}
	}

	if (you.getHP() <= 0) {
		cout << "================================================" << endl;
		cout << "Game Over" << endl;
		cout << "================================================" << endl;
		exit(0);
	}
	else {
		cout << "================================================" << endl;
		cout << you.getName() << " wins!" << endl;
		cout << "================================================" << endl;

		you.growth();

		return you;
	}
	
	return you;
}



int main() {
	//Start of the game
	string pName;
	unitType chosenClass = unitType::none;

	cout << "What is your name" << endl;
	cout << "> ";
	cin >> pName;
	cout << "Welcome to the Arena " << pName << endl;

	cout << "Choose your character class. [1] Warrior [2] Assassin [3] Mage : ";
	int choice;
	cin >> choice;

	while (choice > 3 || choice < 1) {
		cout << "Not a valid choice. Choose your character class. [1] Warrior [2] Assassin [3] Mage : ";
		cin >> choice;
	}

	switch (choice) {
	case 1:
		chosenClass = unitType::warrior;
		break;
	case 2:
		chosenClass = unitType::assassin;
		break;
	case 3:
		chosenClass = unitType::mage;
		break;
	}

	player* You = new player(pName, chosenClass);

	cout << "Welcome " << pName << ", Let's begin your adventure." << endl;
	//Player profile
	cout << "------------------------------------------------------------------" << endl;
	cout << "Character profile:" << endl;
	cout << "Name: " << You->getName() << endl;
	cout << "Class: " << You->getClassName() << endl;
	cout << "HP: " << You->getHP() << endl;
	cout << "POW: " << You->getPOW() << endl;
	cout << "VIT: " << You->getVIT() << endl;
	cout << "AGI: " << You->getAGI() << endl;
	cout << "DEX: " << You->getDEX() << endl;

	cout << endl;
	cout << endl;

	namesList* enemyNames = new namesList();

	//Game continue
	string response = "y";
	int ctr = 1;

	do {
		enemy* newEnemy = new enemy(*enemyNames, ctr);

		*You = battle(*You, *newEnemy);

		cout << endl;
		cout << "Would you like to fight another foe? (Any key for yes, \"n\" for no )" << endl;
		cout << "> ";
		cin >> response;
		cout << endl;

		ctr++;

	} while (response != "n" || response != "N");

	You->printStats();

}