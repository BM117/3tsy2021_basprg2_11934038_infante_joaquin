// First Fantasy.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "player.h" 
#include "enemy.h"
#include "namesList.h"

player battle(player you, enemy opponent) {

	cout << "Loaded Player: " << you.getName() << endl;
	you.printStats();
	//execution waiting here
	cout << "Loaded opponent: " << opponent.getName() << endl;
	opponent.printStats();

	cout << endl;

	cout << "The battle begins!" << endl;

	//pause exection

	int dmg = 0;

	if (you.getAGI() >= opponent.getAGI()) {
		//turn cycle favours you
		while (you.getHP() >= 0 && opponent.getHP() >= 0) {
			dmg = 0;

			dmg = you.attack(opponent.getAGI(), opponent.getVIT(), opponent.getClass());

			if (dmg <= 0) {
				cout << "The attack misses!" << endl;
			}
			else {
				cout << you.getName() << " strikes for " << dmg << " damage!" << endl;
			}

			opponent.setHP(dmg);

			if (opponent.getHP() <= 0) {
				break;
			}

			dmg = opponent.attack(you.getAGI(), you.getVIT(), you.getClass());

			if (dmg <= 0) {
				cout << "The attack misses!" << endl;
			}
			else {
				cout << opponent.getName() << " strikes for " << dmg << " damage!";
			}

			you.setHP(dmg);

			cout << you.getName() << " HP: " << you.getHP();
			cout << opponent.getName() << " HP: " << opponent.getHP();
		}
	}
	else {
		//turn cycle favours enemy

		while (opponent.getHP() >= 0 && you.getHP() >= 0) {
			dmg = 0;

			dmg = opponent.attack(you.getAGI(), you.getVIT(), you.getClass());

			if (dmg <= 0) {
				cout << "The attack misses!" << endl;
			}
			else {
				cout << opponent.getName() << " strikes for " << dmg << " damage!" << endl;
			}

			you.setHP(dmg);

			if (you.getHP() <= 0) {
				break;
			}

			dmg = you.attack(opponent.getAGI(), opponent.getVIT(), opponent.getClass());

			if (dmg <= 0) {
				cout << "The attack misses!" << endl;
			}
			else {
				cout << you.getName() << " strikes for " << dmg << " damage!";
			}

			opponent.setHP(dmg);

			cout << opponent.getName() << " HP: " << opponent.getHP();
			cout << you.getName() << " HP: " << you.getHP();
		}
	}

	return you;
}



int main() {

	string pName;
	unitType chosenClass = unitType::none;

	cout << "What is your name" << endl;
	cout << "> ";
	cin >> pName;
	cout << "Welcome to the Arena " << pName << endl;

	cout << "Choose your character class. [1] Warrior [2] Assassin [3] Mage : ";
	int choice;
	cin >> choice;

	while (choice > 3 || choice < 1) {
		cout << "Not a valid choice. Choose your character class. [1] Warrior [2] Assassin [3] Mage : ";
		cin >> choice;
	}

	switch (choice) {
	case 1:
		chosenClass = unitType::warrior;
		break;
	case 2:
		chosenClass = unitType::assassin;
		break;
	case 3:
		chosenClass = unitType::mage;
		break;
	}

	player* You = new player(pName, chosenClass);

	cout << "Welcome " << pName << ", Let's begin your adventure." << endl;

	cout << "------------------------------------------------------------------" << endl;
	cout << "Character profile:" << endl;
	cout << "Name: " << You->getName() << endl;
	cout << "Class: " << You->getClassName() << endl;
	cout << "HP: " << You->getHP() << endl;
	cout << "POW: " << You->getPOW() << endl;
	cout << "VIT: " << You->getVIT() << endl;
	cout << "AGI: " << You->getAGI() << endl;
	cout << "DEX: " << You->getDEX() << endl;

	cout << endl;
	cout << endl;

	namesList* enemyNames = new namesList();

	enemy* newEnemy = new enemy(*enemyNames);

	cout << "[DEBUG]: Enemyname: " << newEnemy->getName();

	*You = battle(*You, *newEnemy);

	
	string continueGen = "n";
	cout << "DEBUG: Mob generation testing" << endl;
	do {
		enemy* testMob = new enemy(*enemyNames);

		cout << "Generated Enemy: " << endl;
		cout << "Name: " << testMob->getName() << endl;
		cout << "Class: " << testMob->getClassName() << endl;
		cout << "HP: " << testMob->getHP() << endl;
		cout << "POW: " << testMob->getPOW() << endl;
		cout << "VIT: " << testMob->getVIT() << endl;
		cout << "AGI: " << testMob->getAGI() << endl;
		cout << "DEX: " << testMob->getDEX() << endl;
		cout << endl;
		cout << "Generate another? " << endl;
		cin >> continueGen;

	} while (continueGen != "n");
	
}
