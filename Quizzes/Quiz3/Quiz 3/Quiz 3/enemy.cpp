#include "enemy.h"

enemy::enemy(namesList allNames) {
	//Base Stats for all enemy classes
	eHP = 60;
	ePOW = 3;
	eVIT = 3;
	eAGI = 3;
	eDEX = 3;
	//Stat bonuses for all enemy classes
	int classpicker = rand() % 3 + 1;
	cout << "DEBUG: Classpicker = " << classpicker << endl;
	switch (classpicker) {
	case 1:
		eClass = unitType::warrior;
		eHP += 20;
		ePOW += 1;
		eVIT += 2;

		break;
	case 2:
		eClass = unitType::assassin;
		ePOW += 2;
		eAGI += 2;
		eDEX += 1;

		break;
	case 3:
		eClass = unitType::mage;
		ePOW += 3;
		eDEX += 2;
		break;
	}

	//Random Name picker
	int namepicker = rand() % 15;

	switch (eClass) {
	case unitType::warrior:
		eName = allNames.getWarriorNames()[namepicker];
		break;
	case unitType::assassin:
		eName = allNames.getAssassinNames()[namepicker];
		break;
	case unitType::mage:
		eName = allNames.getMageNames()[namepicker];
		break;
	default:
		eName = "Bob";
		break;
	}
}

string enemy::getName()
{
	return eName;
}

unitType enemy::getClass()
{
	return eClass;
}
//Gets the enemy's class
string enemy::getClassName()
{
	switch (eClass) {
	case unitType::warrior:
		return "Warrior";
		break;
	case unitType::mage:
		return "Mage";
		break;
	case unitType::assassin:
		return "Assassin";
		break;
	default:
		return "How did you even...";
	}
}
//getter for enemy stats
int enemy::getHP()
{
	return eHP;
}

int enemy::getPOW()
{
	return ePOW;
}

int enemy::getVIT()
{
	return eVIT;
}

int enemy::getAGI()
{
	return eAGI;
}

int enemy::getDEX()
{
	return eDEX;
}

void enemy::setHP(int reducValue)
{
	eHP -= reducValue;
}
//Displays enemy's stats
void enemy::printStats()
{
	cout << "Class: " << getClassName() << endl;
	cout << "HP: " << eHP << endl;
	cout << "POW: " << ePOW << endl;
	cout << "VIT: " << eVIT << endl;
	cout << "AGI: " << eAGI << endl;
	cout << "DEX: " << eDEX << endl;
}
//Enemy buffs and debuffs
int enemy::attack(int pAGI, int pVIT, unitType pClass)
{
	int hitRate = (eDEX / pAGI) * 100;
	int dmg = ePOW - pVIT;

	switch (eClass) {
	case unitType::warrior:
		if (pClass == unitType::assassin) {
			dmg *= 1.5;
		}
		else if (pClass == unitType::mage) {
			dmg *= 0.5;
		}
		break;

	case unitType::assassin:
		if (pClass == unitType::warrior) {
			dmg *= 0.5;
		}
		else if (pClass == unitType::mage) {
			dmg *= 1.5;
		}
		break;

	case unitType::mage:
		if (pClass == unitType::warrior) {
			dmg *= 1.5;
		}
		else if (pClass == unitType::assassin) {
			dmg *= 0.5;
		}
		break;
	}


	if (hitRate > 80) {
		hitRate = 80;
	}
	else if (hitRate < 20) {
		hitRate = 20;
	}

	cout << "[DEBUG] Hitrate: " << hitRate << endl;
	cout << "[DEBUG] Damage: " << dmg << endl;

	if (rand() % 100 + 1 > hitRate) {
		dmg = 0;
	}

	if (dmg <= 0) {
		dmg = 1;
	}

	return dmg;
}