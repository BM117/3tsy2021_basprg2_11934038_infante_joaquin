#pragma once

#include "namesList.h"
#include "classEnum.h"
#include <iostream>

using namespace std;

class enemy
{
private:
	//Data Members
	string eName;
	unitType eClass;

	int eHP;
	int ePOW;
	int eVIT;
	int eAGI;
	int eDEX;

public:
	//Methods
	enemy(namesList allNames);

	//Getters
	string getName();
	unitType getClass();
	string getClassName();
	int getHP();
	int getPOW();
	int getVIT();
	int getAGI();
	int getDEX();

	//Setters
	void setHP(int reducValue);
	void setPOW(int newPOW);
	void setVIT(int newVIT);
	void setAGI(int newAGI);
	void setDEX(int newDEX);


	//printer
	void printStats();

	//actual actions to actually attack
	int attack(int pAGI, int pVIT, unitType pClass);
};


