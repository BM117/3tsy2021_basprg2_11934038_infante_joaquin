#include "namesList.h"


//List of available names for the enemy and their class
namesList::namesList()
{
	warriorList.push_back("Alra Parasol");
	warriorList.push_back("Automata Girl");
	warriorList.push_back("Bandersnatch Girl");
	warriorList.push_back("Behemoth");
	warriorList.push_back("Cactus Girl");
	warriorList.push_back("Catoblepas Girl");
	warriorList.push_back("Cerberus");
	warriorList.push_back("Chimera Beast");
	warriorList.push_back("Chimera Medullahan");
	warriorList.push_back("Dark Elf Warrior");
	warriorList.push_back("Demon Lamia");
	warriorList.push_back("Walraune");
	warriorList.push_back("Sully");
	warriorList.push_back("Tarantula Girl");
	warriorList.push_back("Yoma");

	mageList.push_back("Alra Priestess");
	mageList.push_back("Aradia");
	mageList.push_back("Astaroth");
	mageList.push_back("Beezlebub");
	mageList.push_back("Brynhildr");
	mageList.push_back("Candle Girl");
	mageList.push_back("Cassandra");
	mageList.push_back("Carbuncle Girl");
	mageList.push_back("Chimera Dryad");
	mageList.push_back("Conquista");
	mageList.push_back("Daji");
	mageList.push_back("Dark Elf Mage");
	mageList.push_back("Zylphe");
	mageList.push_back("Trick Fairy");
	mageList.push_back("Twin Fairies");

	assassinList.push_back("Alra Prison");
	assassinList.push_back("Assassinroid");
	assassinList.push_back("Bee Girl");
	assassinList.push_back("Blonde Centaur");
	assassinList.push_back("Blue Slime");
	assassinList.push_back("Carmilla");
	assassinList.push_back("Chimera Chariot");
	assassinList.push_back("Cobra Girl");
	assassinList.push_back("Dagon");
	assassinList.push_back("Dark Dryad");
	assassinList.push_back("Dark Elf Fencer");
	assassinList.push_back("Demon Monk");
	assassinList.push_back("Tsubasa");
	assassinList.push_back("Wyvern");
	assassinList.push_back("Yao");
}
//Name list vector
vector<string> namesList::getWarriorNames()
{
	return warriorList;
}

vector<string> namesList::getAssassinNames()
{
	return assassinList;
}

vector<string> namesList::getMageNames()
{
	return mageList;
}
