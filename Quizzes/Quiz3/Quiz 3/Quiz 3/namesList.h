#pragma once

#include <vector>
#include <string>

using namespace std;
//Vector for the List of names
class namesList
{
private:
	vector<string> warriorList;
	vector<string> assassinList;
	vector<string> mageList;

public:
	namesList();

	vector<string> getWarriorNames();
	vector<string> getAssassinNames();
	vector<string> getMageNames();
};
