#pragma once
#include <string>
#include <iostream>
#include "classEnum.h"

using namespace std;

class player
{
private:
	//Data Members
	string pName;
	unitType pClass;

	int pHP;
	int pPOW;
	int pVIT;
	int pAGI;
	int pDEX;

public:
	//Constructor
	player(string name, unitType pickedClass);

	//Methods
	void growth();

	//Getters
	string getName();
	unitType getClass();
	string getClassName();
	int getHP();
	int getPOW();
	int getVIT();
	int getAGI();
	int getDEX();

	//Setters
	void setHP(int reducValue);
	void setPOW(int newPOW);
	void setVIT(int newVIT);
	void setAGI(int newAGI);
	void setDEX(int newDEX);

	//printer
	void printStats();

	//actual actions to attack
	int attack(int eAGI, int eVIT, unitType eClass);

};



