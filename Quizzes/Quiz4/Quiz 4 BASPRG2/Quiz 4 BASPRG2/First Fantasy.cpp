#include <iostream>
#include <vector>
#include "player.h"

using namespace std;

player battle(player you) {



	return you;
}


int main() {

	//initialize teams of 3

	vector<player*> battle_flow;

	battle_flow.push_back(new player(unit::DRK, 0));
	battle_flow.push_back(new player(unit::DRG, 0));
	battle_flow.push_back(new player(unit::WHM, 0));

	battle_flow.push_back(new player(unit::Gaius, 1));
	battle_flow.push_back(new player(unit::Varis, 1));
	battle_flow.push_back(new player(unit::White_mage_moogle, 1));

	for (int i = 0; i < battle_flow.size(); i++) {
		if (battle_flow[i]->getTeam() == 0) {
			battle_flow[i]->printStats();
			battle_flow[i]->printMoves();
			cout << endl;
			cout << endl;
		}
	}

	for (int i = 0; i < battle_flow.size(); i++) {
		if (battle_flow[i]->getTeam() == 1) {
			battle_flow[i]->printStats();
			battle_flow[i]->printMoves();
			cout << endl;
			cout << endl;
		}
	}

	int vecsize = battle_flow.size();
	for (int j = 0; j < vecsize - 1; ++j) {

		int min = j;

		for (int i = j + 1; i < vecsize; ++i) {

			if (battle_flow[min]->getAGI() < battle_flow[i]->getAGI()) {
				min = i;
			}

		}
		if (min != j)
			swap(battle_flow[j], battle_flow[min]);
	}

	for (int i = 0; i < battle_flow.size(); i++) {
		battle_flow[i]->printStats();
	}


	//commence battle
}