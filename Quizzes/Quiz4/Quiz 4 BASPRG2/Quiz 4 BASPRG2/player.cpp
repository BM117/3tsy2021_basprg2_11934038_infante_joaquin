#include "player.h"

player::player(unit name, int team) {
	enumName = name;

	//Base stats for all players
	pHP = 50;
	pMaxHP = 50;
	pMP = 20;
	pPOW = 5;
	pVIT = 5;
	pAGI = 5;
	pDEX = 5;

	//assign team
	switch (team) {
	case 0:
		pteam = 0;
		break;
	case 1:
		pteam = 1;
		break;
	}

	//Stat bonuses for each class
	switch (enumName) {
	case unit::DRG:

		pName = "Lo Lilover";

		pPOW += 2;
		pAGI += 3;
		pDEX += 3;

		break;
	case unit::DRK:

		pName = "Complete Weeaboo";

		pMaxHP += 30;
		pHP += 30;
		pPOW += 1;
		pVIT += 3;

		break;
	case unit::WHM:

		pName = "Konpa Keyes";

		pPOW += 5;
		pDEX += 3;
		pMP += 10;

		break;

	case unit::Gaius:

		pName = "Gaius Van Baelsar";

		pPOW += 2;
		pAGI += 3;
		pDEX += 3;

		break;
	case unit::White_mage_moogle:

		pName = "WHM Moogle";

		pPOW += 5;
		pDEX += 3;
		pMP += 10;


		break;
	case unit::Varis:

		pName = "Varis Zos Galvus";

		pMaxHP += 30;
		pHP += 30;
		pPOW += 1;
		pVIT += 3;

		break;
	}

	//moveset initializer
	switch (enumName) {
	case unit::DRG:

		moveList.push_back(new unitMove("True Thrust", 10, moveType::basicAttack, 0));
		moveList.push_back(new unitMove("Stardiver", 10, moveType::aoeSkill, 20));

		break;
	case unit::DRK:

		moveList.push_back(new unitMove("Hard Slash", 10, moveType::basicAttack, 0));
		moveList.push_back(new unitMove("Bloodpsiller", 10, moveType::singleAttack, 20));

		break;
	case unit::WHM:

		moveList.push_back(new unitMove("Glare", 10, moveType::basicAttack, 0));
		moveList.push_back(new unitMove("Cure II", 10, moveType::healSkill, 20));

		break;

	case unit::Gaius:

		moveList.push_back(new unitMove("Hand of the Empire", 10, moveType::basicAttack, 0));
		moveList.push_back(new unitMove("Terminus Est", 10, moveType::singleAttack, 20));

		break;
	case unit::White_mage_moogle:

		moveList.push_back(new unitMove("Stone III", 10, moveType::basicAttack, 0));
		moveList.push_back(new unitMove("Cure II", 10, moveType::healSkill, 20));

		break;
	case unit::Varis:

		moveList.push_back(new unitMove("Citius", 10, moveType::basicAttack, 0));
		moveList.push_back(new unitMove("Vivere Militare Est", 10, moveType::aoeSkill, 20));

		break;
	}

}

string player::getName()
{
	return pName;
}

unit player::getClass()
{
	return enumName;
}

int player::getHP()
{
	return pHP;
}

int player::getMP()
{
	return pMP;
}

int player::getMaxHP() {
	return pMaxHP;
}

int player::getPOW()
{
	return pPOW;
}

int player::getVIT()
{
	return pVIT;
}

int player::getAGI()
{
	return pAGI;
}

int player::getDEX()
{
	return pDEX;
}

int player::getTeam()
{
	return pteam;
}

void player::setHP(int reducValue)
{
	pHP -= reducValue;
}

void player::printStats()
{
	cout << "INFO ================================================" << endl;
	cout << "Name: " << pName << "[HP: " << pHP << "]" << endl;

}

void player::printMoves()
{
	cout << "MOVES ================================================" << endl;
	for (int i = 0; i < moveList.size(); i++) {
		cout << moveList[i]->getMoveName() << "||";
		cout << moveList[i]->getMoveDmg() << "||";
		cout << moveList[i]->getMoveTypeString() << endl;
		cout << "-----------------------------------------" << endl;
	}
	cout << "======================================================" << endl;
}

/*
int player::attack(player pTarget, enemy eTarget, vector<enemy> eTeam)
{
	int hitRate = (pDEX / eTarget.getAGI()) * 100;

	if (hitRate > 80) {
		hitRate = 80;
	}
	else if (hitRate < 20) {
		hitRate = 20;
	}

	//pick move
	cout << "Choose your moves. [1]" << moveList[0]->getMoveName() << " [2] " << moveList[1]->getMoveName() << endl;
	int choice;
	cin >> choice;

	int dmg = 0;

	switch (choice) {
	case 1:
		dmg = pPOW - eTarget.getVIT();
		break;

	case 2:
		dmg = pPOW - eTarget.getVIT();
		if (moveList[1]->getMoveType() == moveType::singleAttack) {
			dmg *= 2.2;
		}
		else if (moveList[1]->getMoveType() == moveType::aoeSkill) {
			dmg *= 0.9;
		}
		break;
	}


	return dmg;
}
*/