#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "classEnum.h"
#include "unitMove.h"

using namespace std;

class player
{
private:
	//Data Members
	string pName;
	unit enumName;

	vector<unitMove*> moveList;

	int pteam;

	int pHP;
	int pMaxHP;
	int pMP;
	int pPOW;
	int pVIT;
	int pAGI;
	int pDEX;

public:
	//Constructor
	player(unit name, int team);

	//Getters
	string getName();
	unit getClass();
	int getHP();
	int getMP();
	int getMaxHP();
	int getPOW();
	int getVIT();
	int getAGI();
	int getDEX();
	int getTeam();

	//Setters
	void setHP(int reducValue);
	void setPOW(int newPOW);
	void setVIT(int newVIT);
	void setAGI(int newAGI);
	void setDEX(int newDEX);

	//printer
	void printStats();
	void printMoves();

	//actual actions
	//int attack(player pTarget, enemy eTarget, vector<enemy> eTeam);

};



