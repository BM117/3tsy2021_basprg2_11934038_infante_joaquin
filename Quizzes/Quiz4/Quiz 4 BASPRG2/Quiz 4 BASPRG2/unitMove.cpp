#include "unitMove.h"

unitMove::unitMove(string mName, int mDmg, moveType mType, int mCost)
{
	name = mName;
	dmg = mDmg;
	type = mType;
	cost = mCost;
}

string unitMove::getMoveName()
{
	return name;
}

int unitMove::getMoveDmg()
{
	return dmg;
}

moveType unitMove::getMoveType()
{
	return type;
}

string unitMove::getMoveTypeString()
{
	switch (type) {
	case moveType::basicAttack:
		return "basic attack";
		break;

	case moveType::aoeSkill:
		return "AoE";
		break;

	case moveType::singleAttack:
		return "Single Attack";
		break;

	case moveType::healSkill:
		return "Healing Skill";
		break;

	default:
		return "N/A";
		break;
	}
}

int unitMove::getCost()
{
	return cost;
}
