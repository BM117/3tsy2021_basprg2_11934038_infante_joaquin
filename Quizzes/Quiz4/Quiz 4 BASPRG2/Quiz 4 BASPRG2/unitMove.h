#pragma once
#include "moveEnum.h"
#include <iostream>

using namespace std;

class unitMove
{
private:
	string name;
	int dmg;
	moveType type;
	int cost;

public:
	unitMove(string mName, int dmg, moveType type, int mCost);

	//getters
	string getMoveName();
	int getMoveDmg();
	moveType getMoveType();
	string getMoveTypeString();
	int getCost();
};
